//! This mod contains the actual unit tests.

mod message;

use prelude::*;

#[fact]
fn macro_test() {
    fact_!(1 + 1).should().be_equal_to(2);
}

#[test]
#[should_panic]
fn a_failure_must_panic() {
    fact_!(1 + 1).should().be_equal_to(3);
}

#[test]
fn success_with_2_assertions() {
    fact_!(1 + 1).should().not().be_equal_to(3)
        .and_should().be_equal_to(2);
}

#[test]
#[should_panic]
fn failure_with_2_assertions_1st_fails() {
    fact_!(1 + 1).should().not().be_equal_to(2)
        .and_should().be_equal_to(2);
}

#[test]
#[should_panic]
fn failure_with_2_assertions_2nd_fails() {
    fact_!(1 + 1).should().not().be_equal_to(3)
        .and_should().be_equal_to(0);
}

#[test]
#[should_panic]
fn failure_with_2_assertions_both_fail() {
    fact_!(1 + 1).should().not().be_equal_to(2)
        .and_should().be_equal_to(0);
}

#[test]
fn success_with_contain() {
    fact_!(&[1, 2, 3]).should().contain(&2);

    fact_!(Some(2)).should().contain(2);

    fact_!(Result::Ok::<_, ()>(2)).should().contain(2);
}

#[test]
fn success_with_be_empty() {
    fact_!(Some(0)).should().not().be_empty();
}

#[test]
fn success_when_error() {
    fact_!(Result::Err::<(), _>(0)).should().be_this_error(0);
}

