//! Contains the various assertions to be used.

use should::Should;
use std::mem::drop;

macro_rules! impl_drop {
    () => {
        fn drop(&mut self) {
            #[allow(unused)]
            use assertions::private::{Implementation, ConsumingImplementation};

            if self.should.is_some() {
                let _ = self.handle_should();
            }
        }
    };
}

mod be_equal_to;
pub use self::be_equal_to::{BeEqualTo, precision::Precision};
mod contain;
pub use self::contain::Contain;
mod be_empty;
pub use self::be_empty::BeEmpty;
mod be_an_error;
pub use self::be_an_error::BeAnError;
mod have_property;
pub use self::have_property::HaveProperty;


/// Adds useful methods to add an explanation to the test and to chain another assertion.
pub trait Assertion: private::Implementation + Sized {
    /// Adds an explanation to the assertion. Note that you cannot chain another assertion
    /// anymore after that, so use it at the end of your assertion(s).
    fn because(mut self, s: &'static str) {
        self.get_should_mut().as_mut().expect(UNWRAP_MSG).info.explanation = Some(s);
        drop(self.handle_should());
    }

    /// Chains another assertion.
    fn and_should(mut self) -> Should<<Self as private::Implementation>::Left> {
        self.handle_should()
    }
}

/// Adds an useful methods to add an explanation to the test.
pub trait ConsumingAssertion: private::ConsumingImplementation + Sized {
    /// Adds an explanation to the assertion.
    fn because(mut self, s: &'static str) {
        self.get_should_mut().as_mut().expect(UNWRAP_MSG).info.explanation = Some(s);
        drop(self.handle_should());
    }
}

impl<T> Assertion for T where T: private::Implementation + Sized {}
impl<T> ConsumingAssertion for T where T: private::ConsumingImplementation + Sized {}

mod private {
    use std::fmt::Debug;
    use should::Should;
    use display::strong;

    pub trait Implementation: Sized {
        type Left: Debug;

        /// Used to access to the `should` field (an "abstract" field would be better,
        /// but it doesn't still exist in Rust…)
        fn get_should_mut(&mut self) -> &mut Option<Should<Self::Left>>;

        /// Creates the message to return when the assertion failed.
        fn message(&self, stringified: Option<String>, left_dbg: &str, truthness: bool) -> String;

        /// Sayes whether the assertion has failed or not.
        fn failed(&self) -> bool;

        /// Computes the next should in case of chaining.
        fn handle_should(&mut self) -> Should<Self::Left> {
            let failed = self.failed();
            let Should { left, truthness, mut info }
                = self.get_should_mut().take().expect(super::UNWRAP_MSG);
            if failed {
                let stringified = info.from_macro.as_ref()
                    .map(|x| strong(x.stringified).to_string());
                let message = self.message(stringified, &info.left_dbg, truthness);
                info.add_message(message);
            }
            Should {
                truthness: true,
                left,
                info,
            }
        }
    }

    pub trait ConsumingImplementation: Sized {
        type Left: Debug;

        /// Used to access to the `should` field (an "abstract" field would be better,
        /// but it doesn't still exist in Rust…)
        fn get_should_mut(&mut self) -> &mut Option<Should<Self::Left>>;

        /// Creates the message to return when the assertion failed.
        fn message(&self, stringified: Option<String>, left_dbg: &str, truthness: bool) -> String;

        /// Sayes whether the assertion has failed or not.
        fn failed(&mut self) -> bool;

        /// Computes the next should in case of chaining.
        fn handle_should(&mut self) {
            let failed = self.failed();
            let Should { left, truthness, mut info }
                = self.get_should_mut().take().expect(super::UNWRAP_MSG);
            let _ = left;
            if failed {
                let stringified = info.from_macro.as_ref()
                    .map(|x| strong(x.stringified).to_string());
                let message = self.message(stringified, &info.left_dbg, truthness);
                info.add_message(message);
            }
        }
    }
}

const UNWRAP_MSG: &'static str = "[fluid] Assertion should have a should member";
