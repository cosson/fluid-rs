use num_traits::Float;
use std::fmt::{Debug, Display};
use theory::{CustomUnwrap, Theory};
use display::*;

#[derive(Debug)]
pub struct BeEqualToPrecision<L>
where
    L: Debug + Display + Float,
{
    pub(crate) theory: Option<Theory<L>>,
    pub(crate) right: L,
    pub(crate) precision: L,
}

impl<L> Drop for BeEqualToPrecision<L>
where
    L: Debug + Display + Float,
{
    impl_drop!();
}

impl<L> BeEqualToPrecision<L>
where
    L: Debug + Display + Float,
{
    impl_assertion!(Theory<L>);
    impl_chain!(Theory<L>);

    /// The message displayed when the assertion failed.
    fn message(&self, left_lit: &str, left_dbg: &str, truthness: bool) -> String {
        format!(
            "\t{} has the value {}\n\
            \tbut it should{} be equal to {} within the range {}.",
            left_lit, left_dbg,
            truthness.str(), strong(&self.right.to_string()), strong(&self.precision.to_string())
        )
    }

    /// Says whether the assertion pass or not.
    fn failed(&self) -> bool {
        let theory = self.theory.unwr_ref();
        ((theory.left - self.right).abs() < self.precision) != theory.truthness
    }
}
