use std::fmt::Debug;
use display::*;
use should::Should;
use std::ops::Not;
use assertions::{private, UNWRAP_MSG};

/// Used to check if a result is an error.
#[derive(Debug)]
pub struct BeAnError<O: Debug, E: Debug> {
    pub(crate) should: Option<Should<Result<O, E>>>,
}

impl<O: Debug, E: Debug> Drop for BeAnError<O, E> {
    impl_drop!();
}

impl<O: Debug, E: Debug> private::Implementation for BeAnError<O, E> {
    type Left = Result<O, E>;

    fn get_should_mut(&mut self) -> &mut Option<Should<Self::Left>> {
        &mut self.should
    }

    fn message(&self, stringified: Option<String>, left_dbg: &str, truthness: bool) -> String {
        if let Some(stringified) = stringified {
            format!(
                "\t{} is {}\n\
                \tbut it should{} be an error.",
                stringified, left_dbg,
                truthness.str()
            )
        } else {
            format!("\t{} is{} an error.", left_dbg, truthness.not().str())
        }
    }

    fn failed(&self) -> bool {
        let should = self.should.as_ref().expect(UNWRAP_MSG);
        should.left.is_err() != should.truthness
    }
}

impl<O: Debug, E: Debug> Should<Result<O, E>> {
    /// Checks if a result is an error.
    ///
    /// # Example
    ///
    /// ```rust
    /// # use fluid::prelude::*;
    /// let result = "two".parse::<i32>();
    /// result.should().be_an_error()
    ///     .because("“two“ is not a valid number");
    /// ```
    pub fn be_an_error(self) -> BeAnError<O, E> {
        BeAnError {
            should: Some(self),
        }
    }
}
