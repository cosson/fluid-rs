use std::fmt::Debug;
use should::Should;
use display::*;
use assertions::{private::ConsumingImplementation, UNWRAP_MSG};
use std::ops::Not;

/// Used to check if an iterator is empty.
#[derive(Debug)]
pub struct BeEmpty<L: Debug>
where
    L: Iterator,
{
    pub(crate) should: Option<Should<L>>,
}

impl<L: Debug> Drop for BeEmpty<L>
where
    L: Iterator,
{
    impl_drop!();
}

impl<L: Debug> ConsumingImplementation for BeEmpty<L>
where
    L: Iterator,
{
    type Left = L;

    fn get_should_mut(&mut self) -> &mut Option<Should<L>> {
        &mut self.should
    }

    fn message(&self, stringified: Option<String>, left_dbg: &str, truthness: bool) -> String {
        if let Some(stringified) = stringified {
            if truthness {
                format!(
                    "\t{} is not empty: {}\n\
                    \tbut it should.",
                    stringified, left_dbg
                )
            } else {
                format!(
                    "\t{} is empty: {}\n\
                    \tbut it should not.",
                    stringified, left_dbg
                )
            }
        } else {
            format!("\t{} is{} empty.", left_dbg, truthness.not().str())
        }
    }

    fn failed(&mut self) -> bool {
        let should = self.should.as_mut().expect(UNWRAP_MSG);
        should.left.next().is_none()
            != should.truthness
    }
}

impl<L: Debug> Should<L>
where
    L: IntoIterator,
    L::IntoIter: Debug,
{
    /// Checks if an empty iterator is empty (consuming assertion).
    ///
    /// # Example
    ///
    /// ```rust
    /// # use fluid::prelude::*;
    ///
    /// Some(2).should().not().be_empty();
    ///
    /// let empty_array = [0; 0];
    /// (&empty_array).should().be_empty();
    /// ```
    pub fn be_empty(self) -> BeEmpty<L::IntoIter> {
        let Should { left, truthness, info } = self;
        let should = Should {
            truthness, info,
            left: left.into_iter(),
        };
        BeEmpty {
            should: Some(should),
        }
    }
}
