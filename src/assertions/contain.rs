use std::fmt::Debug;
use should::Should;
use display::*;
use assertions::{private::ConsumingImplementation, UNWRAP_MSG};
use std::ops::Not;

/// Used to check if an iterator contains an item.
#[derive(Debug)]
pub struct Contain<L: Debug, R: Debug>
where
    L: Iterator,
    L::Item: PartialEq<R> + Debug,
{
    pub(crate) should: Option<Should<L>>,
    pub(crate) right: R,
}

impl<L: Debug, R: Debug> Drop for Contain<L, R>
where
    L: Iterator,
    L::Item: PartialEq<R> + Debug,
{
    impl_drop!();
}

impl<L: Debug, R: Debug> ConsumingImplementation for Contain<L, R>
where
    L: Iterator,
    L::Item: PartialEq<R> + Debug,
{
    type Left = L;

    fn get_should_mut(&mut self) -> &mut Option<Should<L>> {
        &mut self.should
    }

    fn message(&self, stringified: Option<String>, left_dbg: &str, truthness: bool) -> String {
        let right_dbg = self.right.dbg();
        if let Some(stringified) = stringified {
            if truthness {
                format!(
                    "\t{} does not contain {}: {}\n\
                    \tbut it should.",
                    stringified, right_dbg, left_dbg
                )
            } else {
                format!(
                    "\t{} contains {}: {}\n\
                    \tbut it should not.",
                    stringified, right_dbg, left_dbg
                )
            }
        } else {
            format!("\t{} does{} contain {}.", left_dbg, truthness.not().str(), right_dbg)
        }
    }

    fn failed(&mut self) -> bool {
        let should = self.should.as_mut().expect(UNWRAP_MSG);
        let right = &self.right;
        should.left.by_ref().any(|left| left == *right)
            != should.truthness
    }
}

impl<L: Debug> Should<L>
where
    L: IntoIterator,
    L::IntoIter: Debug,
{
    /// Checks if an iterator contains the right field (consuming assertion).
    ///
    /// # Example
    ///
    /// ```rust
    /// # use fluid::prelude::*;
    /// Some(2).should().contain(2);
    /// (&[1, 2, 3]).should().contain(&2);
    /// ```
    pub fn contain<R: Debug>(self, right: R) -> Contain<L::IntoIter, R>
    where
        L::Item: PartialEq<R> + Debug,
    {
        let Should { left, truthness, info } = self;
        let should = Should {
            truthness, info,
            left: left.into_iter(),
        };
        Contain {
            should: Some(should),
            right,
        }
    }
}
