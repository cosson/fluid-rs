pub mod precision;

use self::precision::Precision;
use num_traits::Float;
use std::fmt::{Debug, Display};
use display::*;
use should::Should;
use std::ops::Not;
use assertions::{private, UNWRAP_MSG};

/// Used to check equality between left and right.
#[derive(Debug)]
pub struct BeEqualTo<L: Debug, R: Debug>
where
    L: PartialEq<R>,
{
    pub(crate) should: Option<Should<L>>,
    pub(crate) right: R,
}

impl<L: Debug, R: Debug> Drop for BeEqualTo<L, R>
where
    L: PartialEq<R>,
{
    impl_drop!();
}

impl<L: Debug, R: Debug> private::Implementation for BeEqualTo<L, R>
where
    L: PartialEq<R>
{
    type Left = L;

    fn get_should_mut(&mut self) -> &mut Option<Should<L>> {
        &mut self.should
    }

    fn message(&self, stringified: Option<String>, left_dbg: &str, truthness: bool) -> String {
        let right_dbg = self.right.dbg().to_string();
        if let Some(stringified) = stringified {
            if left_dbg != right_dbg {
                format!(
                    "\t{} has the value {}.\n\
                    \tIt should{} be equal to {} but it is{}.",
                    stringified, left_dbg,
                    truthness.str(), right_dbg, truthness.not().str()
                )
            } else {
                format!(
                    "\t{} is equal to {}\n\
                    \tbut it should{}.",
                    stringified, left_dbg,
                    truthness.str()
                )
            }
        } else {
            format!("\t{} is{} equal to {}.", left_dbg, truthness.not().str(), right_dbg)
        }
    }

    fn failed(&self) -> bool {
        let should = self.should.as_ref().expect(UNWRAP_MSG);
        (should.left == self.right) != should.truthness
    }
}

impl<T> BeEqualTo<T, T>
where
    T: Debug + Display + Float,
{
    /// Modifier for an equality assertion:
    /// checks if a float equality pass within the given precicion.
    ///
    /// # Example
    ///
    /// ```rust
    /// # use fluid::prelude::*;
    /// (1.).should().be_equal_to(1.01).with_precision(0.1);
    /// ```
    pub fn with_precision(mut self, precision: T) -> Precision<T> {
        let should = ::std::mem::replace(&mut self.should, None);
        Precision {
            right: self.right,
            should,
            precision,
        }
    }
}

impl<L: Debug> Should<L> {
    /// Checks if the left field is equal to the right field.
    ///
    /// # Example
    ///
    /// ```rust
    /// # use fluid::prelude::*;
    /// (1 + 1).should().be_equal_to(2);
    /// ```
    pub fn be_equal_to<R: Debug>(self, right: R) -> BeEqualTo<L, R>
    where
        L: PartialEq<R>,
    {
        BeEqualTo {
            should: Some(self),
            right,
        }
    }
}
