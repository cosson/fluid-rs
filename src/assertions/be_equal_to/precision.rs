use num_traits::Float;
use std::fmt::{Debug, Display};
use should::Should;
use display::*;
use assertions::{private, UNWRAP_MSG};
use std::ops::Not;

/// Used to check equality between two float numbers, but with a specific precision.
#[derive(Debug)]
pub struct Precision<L: Debug>
where
    L: Display + Float,
{
    pub(in super) should: Option<Should<L>>,
    pub(in super) right: L,
    pub(in super) precision: L,
}

impl<L: Debug> Drop for Precision<L>
where
    L: Display + Float,
{
    impl_drop!();
}

impl<L: Debug> private::Implementation for Precision<L>
where
    L: Display + Float,
{
    type Left = L;

    fn get_should_mut(&mut self) -> &mut Option<Should<L>> {
        &mut self.should
    }

    fn message(&self, stringified: Option<String>, left_dbg: &str, truthness: bool) -> String {
        let right_dbg = self.right.dbg();
        let precision = self.precision.dbg();
        if let Some(stringified) = stringified {
            format!(
                "\t{} has the value {}\n\
                \tbut it should{} be equal to {} within the range {}.",
                stringified, left_dbg,
                truthness.str(), right_dbg, precision
            )
        } else {
            format!("\t{} is{} equal to {} within the range {}.",
                left_dbg, truthness.not().str(), right_dbg, precision)
        }
    }

    fn failed(&self) -> bool {
        let should = self.should.as_ref().expect(UNWRAP_MSG);
        ((should.left - self.right).abs() < self.precision) != should.truthness
    }
}
