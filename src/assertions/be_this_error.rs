use std::fmt::Debug;
use theory::{CustomUnwrap, Theory};
use display::*;

#[derive(Debug)]
pub struct BeThisError<O, E>
where
    O: Debug,
    E: Debug + PartialEq<E>,
{
    pub(crate) theory: Option<Theory<Result<O, E>>>,
    pub(crate) right: E,
}

impl<O, E> BeThisError<O, E>
where
    O: Debug,
    E: Debug + PartialEq<E>,
{
    impl_assertion!(Theory<Result<O, E>>);
    impl_chain!(Theory<Result<O, E>>);

    fn message(&self, left_lit: &str, left_dbg: &str, truthness: bool) -> String {
        if truthness {
            format!(
                "\t{} is {}\n\
                 \tbut it should be this error: {}.",
                left_lit, left_dbg, self.right.dbg()
            )
        } else {
            format!(
                "\t{} is {}\n\
                \tbut it should not.",
                left_lit, left_dbg
            )
        }
    }

    fn failed(&self) -> bool {
        let theory = self.theory.unwr_ref();
        match &theory.left {
            Ok(_) => true,
            Err(e) => (e == &self.right) != theory.truthness,
        }
    }
}

impl<O, E> Drop for BeThisError<O, E>
where
    O: Debug,
    E: Debug + PartialEq<E>,
{
    impl_drop!();
}

impl<O, E> Theory<Result<O, E>>
where
    E: Debug + PartialEq<E>,
    O: Debug,
{
    /// Checks if the left field is a result that is an error equal
    /// to the right field.
    ///
    /// # Example
    ///
    /// ```rust
    /// use fluid::*;
    ///
    /// let parse_error = match "?".parse::<i32>() {
    ///     Ok(_) => unimplemented!(),
    ///     Err(e) => e,
    /// };
    /// let result = "two".parse::<i32>();
    /// theory!(result).should().be_this_error(parse_error);
    /// ```
    pub fn be_this_error(self, right: E) -> BeThisError<O, E> {
        BeThisError {
            theory: Some(self),
            right,
        }
    }
}
