use std::fmt::Debug;
use display::*;
use should::Should;
use std::ops::Not;
use std::cell::RefCell;
use assertions::{private, UNWRAP_MSG};

/// Used to check if left has some property.
#[derive(Debug)]
pub struct HaveProperty<L: Debug, R>
where
    R: FnOnce(&L) -> bool,
{
    pub(crate) should: Option<Should<L>>,
    pub(crate) right: RefCell<Option<R>>,
}

impl<L: Debug, R> Drop for HaveProperty<L, R>
where
    R: FnOnce(&L) -> bool,
{
    impl_drop!();
}

impl<L: Debug, R> private::Implementation for HaveProperty<L, R>
where
    R: FnOnce(&L) -> bool,
{
    type Left = L;

    fn get_should_mut(&mut self) -> &mut Option<Should<L>> {
        &mut self.should
    }

    fn message(&self, stringified: Option<String>, left_dbg: &str, truthness: bool) -> String {
        if let Some(stringified) = stringified {
            format!(
                "\t{} does{} have the required property: {}\n\
                \tbut it should{}.",
                stringified, truthness.not().str(), left_dbg,
                truthness.str()
            )
        } else {
            format!("\t{} does{} have the required property.", left_dbg, truthness.not().str())
        }
    }

    fn failed(&self) -> bool {
        let should = self.should.as_ref().expect(UNWRAP_MSG);
        (self.right.replace(None).unwrap())(&should.left) != should.truthness
    }
}

impl<L: Debug> Should<L> {
    /// Checks if a user-defined property is satisfied.
    /// 
    /// # Example
    ///
    /// ```rust
    /// # use fluid::prelude::*;
    /// fn an_even_number() -> i32 { 2 }
    /// 
    /// an_even_number().should().have_the_property(|&n| n % 2 == 0);
    /// ```
    pub fn have_the_property<R>(self, right: R) -> HaveProperty<L, R>
    where
        R: FnOnce(&L) -> bool,
    {
        HaveProperty {
            should: Some(self),
            right: RefCell::new(Some(right)),
        }
    }
}
