use std::fmt::Debug;
use colored::{Colorize, ColoredString};

static MAX_DISP_SIZE: Option<usize> = Some(32);

/*
 * PrintDebug
 */

pub trait PrintDebug {
    fn dbg(&self) -> String;
}

impl<T> PrintDebug for T
where
    T: Debug
{
    fn dbg(&self) -> String {
        //let max = ::std::env::var(super::FLUID_MAX_SIZE)
        //    .and_then(|s| s.parse::<u32>())
        //    .unwrap_or(32);
        let s = format!("{:?}", self);
        if let Some(n) = MAX_DISP_SIZE {
            let len = s.chars().count();
            if len > n {
                let start = n * 3 / 4;
                let skip = len - n;
                let mut it = s.chars();
                let start: String = it.by_ref().take(start).collect();
                let end: String = it.skip(skip).collect();

                format!("{} (…) {}", strong(&start), strong(&end))
            } else {
                strong(&s).to_string()
            }
        } else {
            strong(&s).to_string()
        }
    }
}

/*
 * strong
 */

pub fn strong(s: &str) -> ColoredString {
    s.bright_yellow()
}

/*
 * Str
 */

pub trait Str {
    fn str(self) -> &'static str;
}

impl Str for bool {
    fn str(self) -> &'static str {
        match self {
            true => "",
            false => " not",
        }
    }
}
