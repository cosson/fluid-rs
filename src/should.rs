//! Contains the data to create an assertion.
//! 
//! See the [wiki] or the [`Should`](struct.Should.html) struct's methods for more information.
//! 
//! [wiki]: https://gitlab.com/Boiethios/fluid-rs/wikis/assertions

use std::fmt::Debug;
use std::ops::Not;
use display::PrintDebug;
use information::Information;

/// Adds the `should` extension that permits to initiate an assertion.
pub trait ShouldExtension<L: Debug> {
    /// Initiates an assertion. Note that this method should not be used alone, but:
    /// 
    /// - either with the `fact` or `theory` attribute,
    /// - or with the `fact_!` macro.
    fn should(self) -> Should<L>;
}

/// Main data structure used to create assertions.
/// See its methods for more information about the existing assertions.
#[derive(Debug)]
#[must_use]
pub struct Should<L>
where
    L: Debug,
{
    /// The left part of the assertion.
    pub(crate) left: L,

    /// If the assertion must be either true or false.
    pub(crate) truthness: bool,

    /// The infos needed to the right formatting of the final message.
    pub(crate) info: Information,
}


impl<L> Not for Should<L>
where
    L: Debug,
{
    type Output = Self;

    fn not(self) -> Self {
        let Should { left, truthness, info } = self;

        Should {
            truthness: truthness.not(),
            left,
            info,
        }
    }
}

impl<L: Debug> ShouldExtension<L> for L {
    fn should(self) -> Should<L> {
        let left_dbg = self.dbg().to_string();
        Should {
            left: self,
            truthness: true,
            info: Information::new(None, left_dbg),
        }
    }
}
