//! [![Crates.io](https://img.shields.io/crates/v/fluid.svg)](https://docs.rs/fluid) [![wiki](https://img.shields.io/badge/documentation-wiki-purple.svg)](https://gitlab.com/Boiethios/fluid-rs/wikis)
//! 
//! `fluid` is an human readable test library.
//! 
//! See the [wiki](https://gitlab.com/Boiethios/fluid-rs/wikis) for more information.

#![forbid(missing_docs)]
#![deny(unused)]
#![forbid(unsafe_code)]
#![forbid(missing_debug_implementations)]

extern crate colored;
extern crate num_traits;

/// Custom attributes `fact` and `theory`.
pub extern crate fluid_attributes;

pub mod assertions;
mod display;
mod information;
pub mod should;
mod left_element;
//#[cfg(test)]
//mod tests;

/// The things you need to import to use the crate.
/// 
/// # Example
/// 
/// ```
/// use fluid::prelude::*;
/// ```
pub mod prelude {
    pub use std::ops::Not;
    pub use should::ShouldExtension;
    pub use assertions::Assertion;
    #[doc(hidden)]
    pub use left_element::LeftElement;
    pub use fluid_attributes::*;

    /// A macro to replace the custom `#[fact]` attribute if you do not want to use it.
    #[macro_export]
    macro_rules! fact_ {
        ($e:expr) => {
            LeftElement::new($e, stringify!($e), concat!(file!(), ":", line!()), None)
        };
    }
    pub use fact_;
}
