# Fluid

[![Crates.io](https://img.shields.io/crates/v/fluid.svg)](https://docs.rs/fluid) [![wiki](https://img.shields.io/badge/documentation-wiki-purple.svg)](https://gitlab.com/Boiethios/fluid-rs/wikis)

`fluid` is an human readable test library.

See the [wiki](https://gitlab.com/Boiethios/fluid-rs/wikis) for more information.
