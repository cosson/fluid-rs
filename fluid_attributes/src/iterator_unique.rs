pub trait IteratorUnique {
    type Item;

    fn unique(self) -> Option<Self::Item>;
}

impl<T> IteratorUnique for T where T: Iterator + Sized {
    type Item = T::Item;

    fn unique(mut self) -> Option<Self::Item> {
        let next = self.next();
        match self.next().is_some() {
            true => None,
            false => next,
        }
    }
}
