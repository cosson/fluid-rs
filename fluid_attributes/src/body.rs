//! Handles the body generation, more specifically, the transformation of
//! the left side of the assertion into an `LeftElement` struct
//! that carries more information.

// ----------------------
//
// a.b().should().be_equal_to(b);
//
//     receiver:        method:
// 1.  a.b().should()   be_equal_to
// 2.  a.b()            should

use quote::ToTokens;
use syn::{parse_quote, Block, Stmt, Expr};

pub fn transform(block: &mut Block, is_a_theory: bool) {
    for stmt in &mut block.stmts {
        match stmt {
            Stmt::Local(_) => (),
            Stmt::Item(_) => (),
            Stmt::Expr(expr) => handle_expr(expr, is_a_theory),
            Stmt::Semi(expr, _) => handle_expr(expr, is_a_theory),
        }
    }
}

fn handle_expr(expr: &mut Expr, is_a_theory: bool) {
    fn handle_left_elem(left: &mut Expr, is_a_theory: bool) {
        let stringified = left.clone().into_token_stream().to_string();
        debug!(stringified);
        let theory_case = if is_a_theory {
            quote!(Some(__case__))
        } else {
            quote!(None)
        };
        let new_left = parse_quote! {
            LeftElement::new(#left, #stringified, concat!(file!(), ":", line!()), #theory_case)
        };
        *left = new_left;
    }

    match expr {
        Expr::Box(expr) => handle_expr(&mut expr.expr, is_a_theory),
        Expr::InPlace(expr) => handle_expr(&mut expr.value, is_a_theory),
        Expr::Array(_) => (),
        Expr::Call(_) => (),
        Expr::MethodCall(expr) => match expr.method.to_string().as_ref() {
                "should" => handle_left_elem(&mut expr.receiver, is_a_theory),
                _ => handle_expr(&mut expr.receiver, is_a_theory)
            },
        Expr::Tuple(_) => (),
        Expr::Binary(_) => (),
        Expr::Unary(_) => (),
        Expr::Lit(_) => (),
        Expr::Cast(_) => (),
        Expr::Type(_) => (),
        Expr::Let(_) => (),
        Expr::If(expr) => {
                transform(&mut expr.then_branch, is_a_theory);
                if let Some((_, expr)) = &mut expr.else_branch {
                    handle_expr(expr, is_a_theory);
                }
            },
        Expr::While(expr) => transform(&mut expr.body, is_a_theory),
        Expr::ForLoop(expr) => transform(&mut expr.body, is_a_theory),
        Expr::Loop(expr) => transform(&mut expr.body, is_a_theory),
        Expr::Match(expr) => expr.arms.iter_mut().for_each(|arm| handle_expr(&mut arm.body, is_a_theory)),
        Expr::Closure(expr) => handle_expr(&mut expr.body, is_a_theory),
        Expr::Unsafe(expr) => transform(&mut expr.block, is_a_theory),
        Expr::Block(expr) => transform(&mut expr.block, is_a_theory),
        Expr::Assign(expr) => handle_expr(&mut expr.right, is_a_theory),
        Expr::AssignOp(_) => (),
        Expr::Field(_) => (),
        Expr::Index(_) => (),
        Expr::Range(_) => (),
        Expr::Path(_) => (),
        Expr::Reference(_) => (),
        Expr::Break(_) => (),
        Expr::Continue(_) => (),
        Expr::Return(_) => (),
        Expr::Macro(_) => (),
        Expr::Struct(_) => (),
        Expr::Repeat(_) => (),
        Expr::Paren(expr) => handle_expr(&mut expr.expr, is_a_theory),
        Expr::Group(expr) => handle_expr(&mut expr.expr, is_a_theory),
        Expr::Try(expr) => handle_expr(&mut expr.expr, is_a_theory),
        Expr::Async(expr) => transform(&mut expr.block, is_a_theory),
        Expr::TryBlock(expr) => transform(&mut expr.block, is_a_theory),
        Expr::Yield(_) => (),
        Expr::Verbatim(_) => (),
    }
}
