/// Macro used to debug the generated tokens. Add the `"debug"` feature to display those.

macro_rules! debug {
    ($e:expr) => {
        #[cfg(feature = "debug")]
        {
            println!("\x1B[32m====================== {} ======================", stringify!($e));
            println!("{}", $e);
            println!("\x1B[32m======================================");
        }
    };
}
