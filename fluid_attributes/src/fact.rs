use body;
use syn::{parse_quote, ItemFn};

pub fn generate(mut input: ItemFn) -> ItemFn {
    // We add the `#[test]` attribute:
    input.attrs.push(parse_quote!(#[test]));
    
    body::transform(&mut input.block, false);

    input
}
