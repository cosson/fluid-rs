extern crate proc_macro;
extern crate proc_macro2 as pm;
#[macro_use] extern crate quote;
extern crate syn;

#[macro_use] mod debug;

mod fact;
mod theory;

mod body;
mod iterator_unique;

use proc_macro::TokenStream;
use syn::{parse_macro_input, ItemFn};
use quote::{quote, ToTokens};

const DOC_MSG: &str = "Read the documentation at: https://gitlab.com/Boiethios/fluid-rs/wikis/home";

/// The `fact` attribute.
/// A fact is a simple concrete fact to be verified.
/// 
/// See the [wiki] for more information.
/// 
/// # Example
/// 
/// ```
/// #[fact]
/// fn cerberus_has_3_heads() {
///     number_of_faces("Cerberus").should().be_equal_to(3)
///         .because("that's how Cerberus is described");
/// }
/// ```
/// 
/// [wiki]: https://gitlab.com/Boiethios/fluid-rs/wikis/fact
#[proc_macro_attribute]
pub fn fact(args: TokenStream, input: TokenStream) -> TokenStream {
    assert!(args.is_empty(), format!("The macro `fact` must have no arguments. \
        If you want to add some test cases, please use a `theory` with `case`s. {}", DOC_MSG));
    let mut output: TokenStream = quote!(#[cfg(test)] #[fact_inner]).into();

    output.extend(input);
    output
}

/// To not doing expensive treatments when not launching the tests, the `fact` attribute expends
/// to a conditional `fact_inner` attribute.
#[doc(hidden)]
#[proc_macro_attribute]
pub fn fact_inner(_args: TokenStream, input: TokenStream) -> TokenStream {
    debug!(input);
    let output = fact::generate(parse_macro_input!(input as ItemFn)).into_token_stream();

    debug!(output);
    output.into()
}


/// The `theory` attribute.
/// A theory is a test more complex than a fact, that must be proven or inferred with multiple cases.
/// Therefore, a theory must come with its multiple test `case`s.
/// 
/// See the [wiki] for more information.
/// 
/// # Example
/// 
/// ```
/// #[theory]
/// #[case("Cerberus", 3)]
/// #[case("Hydra", 7)]
/// #[case("Janus", 2)]
/// #[case("Normal guy", 1)]
/// fn each_cerature_has_a_correct_number_of_faces(name: &str, nbr_faces: u8) {
///     number_of_faces(name).should().be_equal_to(nbr_faces);
/// }
/// ```
/// 
/// [wiki]: https://gitlab.com/Boiethios/fluid-rs/wikis/theory
#[proc_macro_attribute]
pub fn theory(args: TokenStream, input: TokenStream) -> TokenStream {
    assert!(args.is_empty(), format!("The macro `theory` must have no arguments. \
        If you want to add some test cases, please use the `case` attribute. {}", DOC_MSG));
    let mut output: TokenStream = quote!(#[cfg(test)] #[theory_inner]).into();

    output.extend(input);
    output
}

/// To not doing expensive treatments when not launching the tests, the `theory` attribute expends
/// to a conditional `theory_inner` attribute.
#[doc(hidden)]
#[proc_macro_attribute]
pub fn theory_inner(_args: TokenStream, input: TokenStream) -> TokenStream {
    debug!(input);
    let output = theory::generate(parse_macro_input!(input as ItemFn)).into_token_stream();

    debug!(output);
    output.into()
}


/// Dummy `case` attribute to not trigger a compiler error when used with the `theory` attribute.
#[doc(hidden)]
#[proc_macro_attribute]
pub fn case(_args: TokenStream, _input: TokenStream) -> TokenStream {
    panic!("The `case` attribute must be used after a `theory`. {}", DOC_MSG);
}
