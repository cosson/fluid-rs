//! Handles the `theory` tests. The code generation works as following:
//! 
//! ```
//! #[theory]
//! #[case(0, "zero")]
//! #[case(2, "two")]
//! #[case(4, "four")]
//! fn is_even_is_ok<T>(i: int, _s: T) where T: AsRef<str> {
//!     is_even(i).should().be_true();
//! }
//! ```
//! 
//! becomes:
//! ```
//! #[test]
//! fn is_even_is_ok() {
//!     fn inner_func<T>(__case__: &'static str, i: int) where T: AsRef<str> {
//!         LeftElement::new(is_even(i), "is_even(i)", "file:line", Some(__case__)).should().be_true();
//!     }
//!     inner_func("i = 0", 0);
//!     inner_func("i = 2", 2);
//!     inner_func("i = 4", 4);
//! }
//! ```
//! 

use std::cmp::Ordering;
use std::mem::replace;
use syn::{parse_quote, ItemFn, FnDecl, Attribute, punctuated::Punctuated, Path, Ident, Visibility, ReturnType, Pat, FnArg};
use pm::{TokenStream, TokenTree};
use quote::ToTokens;
use body;
use iterator_unique::IteratorUnique;

type FnArgs = Punctuated<syn::FnArg, syn::token::Comma>;
type CallArgs = Punctuated<syn::Expr, syn::token::Comma>;

pub fn generate(mut input: ItemFn) -> ItemFn {
    assert_no_param_has_reserved_name(&input.decl.inputs);
    let cases = extract_cases(&mut input.attrs);
    let stringified = generate_stringified_cases(&input.decl.inputs, &cases);
    let vis = replace(&mut input.vis, Visibility::Inherited);
    let mut attrs = replace(&mut input.attrs, Vec::new());
    let span = input.ident.span().clone();
    let ident = replace(&mut input.ident, Ident::new("inner_func", span));

    input.decl.inputs.push(parse_quote!(__case__: &'static str));
    attrs.push(parse_quote!(#[test]));
    body::transform(&mut input.block, true);
    let decl = FnDecl {
        fn_token: Default::default(),
        generics: Default::default(),
        paren_token: Default::default(),
        inputs: Default::default(),
        variadic: None,
        output: ReturnType::Default,
    };
    let block = parse_quote! {{
        #input
        #(
            inner_func(#cases, #stringified);
        )*
    }};

    ItemFn {
        attrs,
        vis,
        constness: None,
        unsafety: None,
        asyncness: None,
        abi: None,
        ident,
        decl: Box::new(decl),
        block,
    }
}

fn generate_stringified_cases(args: &FnArgs, cases: &[TokenStream]) -> Vec<String> {
    fn token_stream_to_call_args(stream: TokenStream) -> CallArgs {
        parse_quote!(#stream)
    }
    fn fn_arg_to_string(arg: &FnArg) -> String {
        match arg {
            FnArg::Captured(arg) => format!("{}", arg.pat.clone().into_token_stream()),
            _ => "<?>".into(),
        }
    }
    fn join_strings(it: impl IntoIterator<Item = String>) -> String {
        let mut it = it.into_iter();
        let first = it.next().expect("There should be at least one case");

        it.fold(first, |acc, s| acc + ", " + &s)
    }

    cases.iter().cloned().map(token_stream_to_call_args).map(|case| {
        let strings = case.iter().zip(args.iter()).map(|(val, arg)| {
            let val = val.into_token_stream();
            let arg = fn_arg_to_string(arg);

            format!("{} = {}", arg, val)
        });

        join_strings(strings)
    }).collect()
}

fn assert_no_param_has_reserved_name(args: &FnArgs) {
    fn check_pattern(pat: &Pat) {
        match pat {
            Pat::Ident(ident) => if ident.ident.to_string() == "__case__" {
                    panic!("`__case__` is a reserved parameter identifier. Please chose another one.")
                },
            Pat::Struct(struc) => struc.fields.iter().map(|f| f.pat.as_ref()).for_each(check_pattern),
            _ => (), // TODO: the other ones
        }
    }
    use syn::FnArg::*;

    for arg in args.iter() {
        let pattern = match arg {
            Captured(arg) => &arg.pat,
            Inferred(pat) => pat,
            _ => continue,
        };
        check_pattern(pattern);
    }
}

fn extract_cases(attrs: &mut Vec<Attribute>) -> Vec<TokenStream> {
    fn to_parameters(attr: Attribute) -> TokenStream {
        match attr.tts.into_iter().unique() {
            Some(TokenTree::Group(group)) => group.stream(),
            _ => panic!("A case must have the form: `#[case(param1, param2, ...)]`.")
        }
    }

    let case = Path {
        leading_colon: None,
        segments: parse_quote!(case),
    };
    attrs.sort_by(|attr1, attr2| if attr1.path == case {
            Ordering::Greater
        } else if attr2.path == case {
            Ordering::Less
        } else {
            Ordering::Equal
        });
    let to_skip = attrs.iter().take_while(|attr| attr.path != case).count();

    attrs
        .drain(to_skip..)
        .map(to_parameters)
        .collect()
}
